#!/bin/sh

echo "Logging into the Docker Daemon"
docker login -u $DOCKER_LOGIN -p $DOCKER_PASSWORD

echo "Building the Docker Image"
docker build -t contractor-marketplace .

echo "Tagging Image"
IMAGE_ID=$(docker images --format "{{.ID}}" contractor-marketplace)
docker tag $IMAGE_ID swen90013/contractor-marketplace:ci

echo "Pushing to Dockerhub"
docker push swen90013/contractor-marketplace
