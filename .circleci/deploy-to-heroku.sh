#!/bin/sh

docker login -u $DOCKER_LOGIN -p $HEROKU_AUTH_TOKEN registry.heroku.com
heroku container:push web --app $HEROKU_APP_NAME
heroku container:release web --app $HEROKU_APP_NAME
