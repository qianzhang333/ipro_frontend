# Frontend API Documentation

The following component decomposition is categorised into Atoms, Molecules, Organisms and Pages. This taxonmy is based on Brad Frost's Atomic Design System.

Read the Atomic Design Book at http://atomicdesign.bradfrost.com/ for more information.

---
## Atoms
### Badge Component `<Badge />`

## *TBD*

#### Badge Component Props
| Prop Name        | Prop Values / Types          |
| ---------------- | ---------------------------- |
| membership       | { gold, silver, bronze }     |

#### Badge Component Examples
```html
<Badge membership="gold" />
```

---

### CoreButton Component `<CoreButton />`

Used to display a button, only primary and secondary buttons need to be supported in Sprint 1.

In the event multiple priority prop names are specified
1. A warning is outputted to console
2. The button will take the highest specified priority (ie. primary)

#### CoreButton Component Props
| Prop Name     | Prop Values / Types      |
| ------------- | ------------------------ |
| primary       | { true, false }          |
| secondary     | { true, false }          |

#### CoreButton Component Examples
```html
<Button primary>SIGN UP</Button>
<Button secondary>LOGIN</Button>
```
---

### TextInput Component `<TextInput />`

#### TextInput Component Props
| Prop Name        | Prop Values / Types |
| ---------------- | ------------------- |
| tip              | String              |

#### TextInput Component Examples
```html
<TextInput tip="Example Placeholder Tip" />
```

---
### CoreText Component `<CoreText />`

In the event multiple styles are specified
1. A warning is outputted to console
2. The text will take the highest specified style.

#### CoreText Component Props
| Prop Name        | Prop Values / Types |
| ---------------- | ------------------- |
| h1               | { true, false }     |
| h2               | { true, false }     |
| h3               | { true, false }     |
| h4               | { true, false }     |
| h5               | { true, false }     |
| h6               | { true, false }     |
| p                | { true, false }     |

#### CoreText Component Examples
```html
<CoreText h2>This is a H2 styled text</CoreText>
<CoreText p>This is paragraph styled text</CoreText>
```
---
### Link Component `<Link />`

#### Link Component Props
| Prop Name        | Prop Values / Types |
| ---------------- | ------------------- |
| name             | { true, false }     |
| to               | { true, false }     |

#### Link Component Examples
## *TBD*

---
## Molecules

No reusable molecules exist so far.

---
## Organisms
### NavFooter Component `<NavFooter />`
#### NavFooter Component Props

This component does not receive any props.

#### NavFooter Examples
```html
<NavFooter />
```
---
### NavHeader Component `<NavHeader />`

#### NavHeader Component Props
This component does not receive any props.

#### NavHeader Examples
```html
<NavHeader />
```
---
### Search Component `<Search />`

#### Search Component Props
| Prop Name        | Prop Values / Types |
| ---------------- | ------------------- |
| search-tip       | String              |
| location-tip     | String              |
| button-text      | String              |

#### Search Component Examples
```html
<Search
  search-tip="I am looking for..."
  location-tip="Eg. Melbourne"
  button-text="Search"
/>
```
---

## Pages

### ContractorCard Component `<ContractorCard />`

## *TBD*

#### ContractorCard Component Props
| Prop Name        | Prop Values / Types          |
| ---------------- | ---------------------------- |
| contractor-id    | String | Number              |

#### ContractorCard Component Examples
```html
<ContractorCard contractor-id="1" />
```

---
### App Component `<App />`
```html
<App>
	<NavHeader />
	<HomePage | SearchResultsPage | ContractorProfilePage />
	<NavFooter />
</App>
```
---
### ContractorProfilePage Component `<ContractorProfilePage />`
```html
<ContractorProfilePage>
	<ContractorProfileCard />
	<ContractorProfileCard />
	...
	<ContractorProfileCard />
</ContractorProfilePage>
```
---
### HomePage Component `<HomePage />`
```html
<HomePage>
	<Search />
	<Contractors />
	<Clients />
	<ValueProposition />
</HomePage>
```

---
### SearchResultsPage Component `<SearchResultsPage />`
```html
<SearchResultsPage>
	<Search />
	<ResultsCounter />
	<ContractorCardList>
		<ContractorCard />
		<ContractorCard />
		...
		<ContractorCard />
	</ContractorCardList>
	<NavPagination />
</SearchResultsPage>
```

---