FROM node:9.11.1-alpine

# http-server will serve the content of the build
RUN npm install -g http-server

# Set base directory
WORKDIR /app

# Install project dependencies
COPY package*.json ./
RUN npm install

# Copy contents across to image
COPY . .

# Build app for production with minification
RUN npm run build

EXPOSE 8080
CMD [ "http-server", "dist" ]
