# marketplace

> iPRO Contractor Marketplace

## Directory Structure
Contents of the web application will be contained within this repository, along with any additional files required for the web application to run in ci or be deployed to a particular environment.

This repository has been built using the default webpack template from the vue-cli. The structure of this application is as follows:

``` bash
  - /config         # Contains all configuration files for environments etc.
  - /src            # Main contents of the application
      /assets:      # Location of any multimedia assets
  -   /components   # Location of reusable vue components
  - /test
      /unit:        # Home for our Jest Unit Testing Framework.
  -     /specs:     # Location of the unit test .spec files
  -     /coverage:  # Location of coverage report metadata and html files.
  -   /e2e:         # Home for our Nightwatch End to End Testing Framework.
  -     /specs:     # Location of our automated e2e test files.
  - /data           # Mock json data to speed up development locally for the front end team
  - /.circleci      # Configuration files for the CirclCI continuous integration environment
```

## Pre Requisites
The following application requires node v6.9.2 or higher. If you do not have node installed, please do that as a first point of call. If you have a previous version of node installed, you can either upgrade your version of node, using 'brew update node' or you can download nvm https://github.com/creationix/nvm (node version manager) and install multiple versions of node.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run test:unit

# run e2e tests
npm run test:e2e

# run all tests
npm test:all

# Build docker image for the frontend application
# Assumes root directory is the current directory (/swen90013-2018-pr)
docker build -t <INSERT_IMAGE_NAME_HERE:INSERT_TAG_HERE> .

# Run the application using the built docker image
# Assumes the docker image has been built using previous command
# Container, image and tag values all chosen at the discretion of the user
docker run -p 8080:8080 --name <INSERT_CONTAINER_NAME_HERE> <INSERT_IMAGE_NAME_HERE:INSERT_TAG_HERE>
```

## CI Environment
We have chosen to use CircleCI to use as our Continuous Integration environment. The downside with using this tool is that it cannot be linked to our on premise Bitbucket repository. So to trigger the CI build for a particular branch, you must push the branch to our mirror repository hosted on Github [https://github.com/SWEN90013-Contractor-Marketplace/ipro-marketplace.git](https://github.com/SWEN90013-Contractor-Marketplace/ipro-marketplace.git)

An easy way to do this, is to setup another remote variable in your local git project configurations. See below for details on how to do so:

```bash
# See existing remote variables
git remote -v
# Output:
#        origin	https://rokane@bitbucket.cis.unimelb.edu.au:8445/scm/swen90013/swen90013-2018-pr.git (fetch)
#        origin	https://rokane@bitbucket.cis.unimelb.edu.au:8445/scm/swen90013/swen90013-2018-pr.git (push)

# Set another remote variable
git remote add <NAME_YOU_WANT_TO_CALL> https://github.com/SWEN90013-Contractor-Marketplace/ipro-marketplace.git

# Check remotes again
git remote -v
# Output:
#        origin	https://rokane@bitbucket.cis.unimelb.edu.au:8445/scm/swen90013/swen90013-2018-pr.git (fetch)
#        origin	https://rokane@bitbucket.cis.unimelb.edu.au:8445/scm/swen90013/swen90013-2018-pr.git (push)
#        <NAME_OF_REMOTE>	https://github.com/SWEN90013-Contractor-Marketplace/ipro-marketplace.git (fetch)
#        <NAME_OF_REMOTE>	https://github.com/SWEN90013-Contractor-Marketplace/ipro-marketplace.git (push)
```

Once you have setup your git configuration to have multiple remotes, you can simply push the branch you want to run under ci using the below command:

```bash
# Push to Github upstream
git push <NAME_OF_REMOTE> <NAME_OF_BRANCH>
```

Once that is complete, you should be able to navigate to our CircleCI dashboard and see the build status and logs from your build. [CircleCI Dashboard](https://circleci.com/gh/SWEN90013-Contractor-Marketplace/workflows)


For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
