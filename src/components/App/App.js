import NavHeader from '@/components/organisms/NavHeader';
import NavFooter from '@/components/organisms/NavFooter';

export default {
  name: 'App',
  components: {
    NavHeader,
    NavFooter,
  },
};
