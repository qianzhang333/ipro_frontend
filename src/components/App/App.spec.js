import { shallowMount } from '@vue/test-utils';
import App from './';

describe('<App />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(App, {});
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
