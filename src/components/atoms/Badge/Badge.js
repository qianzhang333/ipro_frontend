export default {
  name: 'Badge',
  props: ['membership'],
  methods: {
    imageUrl(membership) {
      const urls = {
        Gold: '../static/img/gold_member.png',
        Silver: '../static/img/silver_member.png',
        Bronze: '../static/img/bronze_member.png',
      };
      return urls[membership];
    },
  },
};
