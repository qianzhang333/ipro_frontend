import { mount } from '@vue/test-utils';
import Badge from './';
import BadgeClass from './Badge';

describe('Badge.vue', () => {
  let wrapper;

  it('has a method that retrieve necessary imageUrls', () => {
    expect(BadgeClass.methods.imageUrl('Gold')).toContain('gold');
    expect(BadgeClass.methods.imageUrl('Silver')).toContain('silver');
    expect(BadgeClass.methods.imageUrl('Bronze')).toContain('bronze');
    expect(BadgeClass.methods.imageUrl('')).toBeUndefined();
  });

  it('matches Gold Membership snapshot', () => {
    wrapper = mount(Badge, {
      propsData: {
        membership: 'Gold',
      },
    });
    expect(wrapper.contains('.badge-img')).toBe(true);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches Silver Membership snapshot', () => {
    wrapper = mount(Badge, {
      propsData: {
        membership: 'Silver',
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches Bronze Membership snapshot', () => {
    wrapper = mount(Badge, {
      propsData: {
        membership: 'Bronze',
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches no Membership snapshot', () => {
    wrapper = mount(Badge, {
      propsData: {
        membership: '',
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches no Membership snapshot, too', () => {
    wrapper = mount(Badge, {
      propsData: {
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
