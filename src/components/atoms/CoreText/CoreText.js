import getBooleanAttribute from '@/utils/propUtils';

const props = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p'];
const defaultProp = 'p';

export default {
  props,
  data() {
    const styleObject = getBooleanAttribute(
      this,
      props,
      defaultProp,
    );
    return {
      styleObject,
    };
  },
};
