import { shallowMount } from '@vue/test-utils';
import CoreText from './';
import styleObject from './CoreText';

describe('<CoreText.vue /> ', () => {
  let wrapper;
  const props = {
    h1: true,
    h2: false,
    h3: false,
    h4: false,
    h5: false,
    h6: false,
    p: false,
  };

  it('checks h1 is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h1: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h1).toEqual(true);
  });

  it('checks h2 is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h2: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h2).toEqual(true);
  });

  it('checks h3 is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h3: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h3).toEqual(true);
  });

  it('checks h4 is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h4: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h4).toEqual(true);
  });

  it('checks h5 is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h5: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h5).toEqual(true);
  });

  it('checks h6 is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h6: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h6).toEqual(true);
  });

  it('checks p is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        p: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.p).toEqual(true);
  });

  it('checks {h1,h2} is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {
      propsData: {
        h1: true,
        h2: true,
      },
      styleObject: props,
    });
    expect(wrapper.vm.styleObject.h1).toEqual(true);
  });

  it('checks {} is in the styleObject', () => {
    wrapper = shallowMount(CoreText, {});
    expect(wrapper.vm.styleObject.p).toEqual(true);
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('import exist', () => {
    expect(styleObject.data() > 0);
  });
});

