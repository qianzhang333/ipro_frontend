import { shallowMount } from '@vue/test-utils';
import Link from './';

describe('<Link.vue /> ', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Link, {
    });
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});

