import { shallowMount } from '@vue/test-utils';
import TextInput from './';

describe('<TextInput.vue /> ', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(TextInput, {});
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});

