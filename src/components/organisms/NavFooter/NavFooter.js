import Link from '@/components/atoms/Link';

export default {
  name: 'NavFooter',
  components: {
    Link,
  },
};
