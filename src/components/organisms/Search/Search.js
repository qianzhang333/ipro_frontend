import router from '@/router';
import { debounce } from 'lodash';
import { businessNameQuery } from '@/utils/searchApi';

const DEBOUNCE_IN_MILLISECONDS = 200;
const QUERY_LIMIT = 5;
function debouncedRequest(fn) {
  return debounce(fn, DEBOUNCE_IN_MILLISECONDS);
}
const hasProperty = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);

export default {
  name: 'Search',
  data() {
    return {
      // The last query request sent to the server
      nextQuery: '',
      // Model for the primary input text
      primaryModel: null,
      // Primary search text input to watch
      primarySearch: null,
      // Visible primary suggestions
      primarySuggestions: [],
      // Results we get back from the server
      // contractor: [],
      // service_category: [],
      // Cached results (via a query string key)
      cache: {},
      // TODO: Location field to be implemented
      locationModel: null,
      locationSearch: null,
      locationSuggestions: [],
    };
  },
  props: [
    'currentLocation',
  ],
  watch: {
    primarySearch(newValue) {
      if (hasProperty(newValue, 'type')) {
        // The user mis-clicked
        router.push({ path: newValue.getLink() });
      } else {
        this.fetchSuggestions(newValue);
      }
    },
  },
  methods: {
    updateCacheEntry(query, newData) {
      if (this.cache[query]) {
        this.primarySuggestions = this.cache[query];
        return;
      }
      const processedContractor = newData.contractor.map((elem) => {
        const clone = Object.assign({}, elem);
        const tradingName = elem.trading_name;
        const contractorId = elem.contractor_id;
        clone.type = 'contractor';
        clone.toString = () => `${tradingName}`;
        clone.getLink = () => `/profile/${contractorId}`;
        clone.getIcon = () => 'business';
        clone.getKey = () => `${tradingName}`;
        return clone;
      });
      const processedServiceCategory = newData.service_category.map((otherElem) => {
        const otherClone = Object.assign({}, otherElem);
        const category = otherElem.category;
        otherClone.type = 'service_category';
        otherClone.toString = () => `${category}`;
        otherClone.getLink = () => `/results/${category}`;
        otherClone.getIcon = () => 'search';
        otherClone.getKey = () => `${category}`;
        return otherClone;
      });
      const processedResults = processedContractor.concat(processedServiceCategory);
      this.cache[query] = processedResults;
      this.primarySuggestions = processedResults;
    },
    fetchSuggestions(query) {
      if (!query && query !== '0') {
        this.primarySuggestions = [];
        return;
      }
      const processedQuery = (
        query.substring(0, 1).toUpperCase() + query.substring(1)
      );
      if (this.cache[processedQuery]) {
        this.primarySuggestions = this.cache[processedQuery];
        return;
      }
      const apollo = this.$apollo;
      debouncedRequest(
        () => {
          this.nextQuery = processedQuery;
          apollo.queries.contractor.setOptions({
            result: this.onSuccess,
          });
          apollo.queries.contractor.refetch({
            searchText: `${processedQuery}%`,
            limit: QUERY_LIMIT,
          });
        },
      )();
    },
    performSearch(query) {
      if (!query) return false;
      router.push({ path: `/results/${query}` });
      // force router to go to the current page
      router.go(0);
      return true;
    },
    onSuccess(response) {
      if (response.loading) return;
      this.updateCacheEntry(this.nextQuery, response.data);
    },
  },
  apollo: businessNameQuery({
    searchText: '',
    limit: QUERY_LIMIT,
    // Prevents unit tests from failing when `this` is undefined
    onSuccess: (this) ? this.onSuccess : () => {},
  }),
};
