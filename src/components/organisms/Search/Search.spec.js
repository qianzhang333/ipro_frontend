
import { mount } from '@vue/test-utils';
import Search from './';

describe('<Search /> ', () => {
  let wrapper;

  beforeEach(() => {
    const onSuccess = jest.fn();
    wrapper = mount(Search, {
      methods: {
        onSuccess,
      },
    });
  });

  it('primary input contains an empty string', () => {
    wrapper.find('#input-primary').setValue('');
    const searchButtonWrapper = wrapper.find('#btn-search');
    searchButtonWrapper.trigger('click');
    expect(wrapper.html()).toMatchSnapshot();
  });

  xit('primary input contains a non-empty string', () => {
    wrapper.find('#input-primary').setValue('electrician');
    const searchButtonWrapper = wrapper.find('#btn-search');
    searchButtonWrapper.trigger('click');
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
