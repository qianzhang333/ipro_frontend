import Badge from '@/components/atoms/Badge';
import CoreButton from '@/components/atoms/CoreButton';

const CARD = 'profile-detail-card';
const CARD_GOLD = 'profile-detail-gold-card';
const CARD_SILVER = 'profile-detail-silver-card';
const CARD_BRONZE = 'profile-detail-bronze-card';

export default {
  name: 'ContractorProfileCard',
  props: ['dataModel'],
  components: {
    Badge,
    CoreButton,
  },
  data() {
    return {
      defaultLogoImg: 'this.src= " ../../../../../static/img/IPRO_logo.png"',
    };
  },
  methods: {
    classObjects() {
      const membershipProps = {
        Gold: CARD_GOLD,
        Silver: CARD_SILVER,
        Bronze: CARD_BRONZE,
      };

      if (membershipProps[this.dataModel.membership]) {
        return membershipProps[this.dataModel.membership];
      }
      return CARD;
    },
  },
};
