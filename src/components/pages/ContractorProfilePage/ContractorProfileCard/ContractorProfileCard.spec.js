import { mount } from '@vue/test-utils';
import ContractorProfileCard from './';

describe('<LandingClients />', () => {
  let wrapper;
  let dataModel;

  beforeEach(() => {
    dataModel = {
      contractor_id: '2',
      countryAddr: 'Australia',
      companyLogo: 'https://image.freepik.com/free-vector/blue-company-logo_1057-513.jpg',
      legalEntityName: 'First Response Pty Ltd',
      tradingName: 'First Response Plumbing',
      serviceCategories: 'Plumbing',
      membership: 'Silver',
      suburbAddr: '1',
      postcode: '2',
      streetAddr: '3',
      numberAddr: '4',
      stateAddr: 'VIC',
      phoneNumber: '417123456',
      email: 'simonp@platform.im',
      status: 'active_non_compliant',
      linkedClients: 'ABC Testing Client Pty Ltd, Clear to Work, , iPRO Client Pty Ltd',
    };
    wrapper = mount(
      ContractorProfileCard,
      {
        propsData: { dataModel },
      },
    );
  });

  it('handles invalid membership values', () => {
    const dataModelWithInvalidMembership = {
      contractor_id: '2',
      countryAddr: 'Australia',
      companyLogo: 'https://image.freepik.com/free-vector/blue-company-logo_1057-513.jpg',
      legalEntityName: 'First Response Pty Ltd',
      tradingName: 'First Response Plumbing',
      serviceCategories: 'Plumbing',
      membership: 'INVALID_MEMBERSHIP~@#$',
      suburbAddr: '1',
      postcode: '2',
      streetAddr: '3',
      numberAddr: '4',
      stateAddr: 'VIC',
      phoneNumber: '417123456',
      email: 'simonp@platform.im',
      status: 'active_non_compliant',
      linkedClients: 'ABC Testing Client Pty Ltd, Clear to Work, , iPRO Client Pty Ltd',
    };
    const otherWrapper = mount(
      ContractorProfileCard,
      {
        propsData: { dataModel: dataModelWithInvalidMembership },
      },
    );
    expect(otherWrapper.html()).toMatchSnapshot();
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
