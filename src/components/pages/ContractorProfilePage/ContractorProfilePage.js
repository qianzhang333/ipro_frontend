import router from '@/router';
import { contractorQuery } from '@/utils/searchApi';
import ContractorProfileCard from './ContractorProfileCard';

function getContractorId() {
  if (router) {
    return router.currentRoute.params.id;
  }
  return undefined;
}

export default {
  name: 'ContractorProfilePage',
  components: {
    ContractorProfileCard,
  },
  data() {
    return {
      contractorId: undefined,
      contractor: [],
      contractorData: undefined,
    };
  },
  watch: {
    contractor(newContractor) {
      this.contractorData = (newContractor) ? newContractor[0] : undefined;
    },
  },
  methods: {
    triggerRefetch() {
      this.$apollo.queries.contractor.refetch({
        contractorId: getContractorId(),
      });
    },
  },
  mounted() {
    this.contractorId = getContractorId();
    this.triggerRefetch();
  },
  apollo: contractorQuery(getContractorId()),
};
