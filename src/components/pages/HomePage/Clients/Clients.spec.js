import { mount } from '@vue/test-utils';
import Clients from './';

describe('<Clients />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(Clients);
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
