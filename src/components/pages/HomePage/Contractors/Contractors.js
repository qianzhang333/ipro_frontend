export default {
  name: 'Contractors',
  methods: {
    categoryLink(serviceCategory) {
      return `/results/${serviceCategory}`;
    },
  },
};
