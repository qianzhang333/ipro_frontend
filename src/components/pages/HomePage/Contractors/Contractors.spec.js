import { shallowMount } from '@vue/test-utils';
import Contractors from './';

describe('<Contractors />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Contractors);
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
