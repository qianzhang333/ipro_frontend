import Search from '@/components/organisms/Search';
import Contractors from './Contractors';
import Clients from './Clients';
import ValueProposition from './ValueProposition';

export default {
  name: 'HomePage',
  components: {
    Search,
    Contractors,
    Clients,
    ValueProposition,
  },
  data() {
    return {
      query: '',
    };
  },
};
