import { shallowMount } from '@vue/test-utils';
import HomePage from './';

describe('HomePage', () => {
  it('renders', () => {
    const wrapper = shallowMount(HomePage);
    expect(wrapper.html()).toMatchSnapshot();
  });
});
