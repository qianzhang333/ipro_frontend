import { shallowMount } from '@vue/test-utils';
import ValueProposition from './';
import topProposition from './ValueProposition';

describe('<ValueProposition.vue /> ', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(ValueProposition, {});
  });

  it('renders each item in the ValueProposition', () => {
    expect(wrapper.findAll('div').at(1).classes()).toContain('topProposition');
    expect(wrapper.findAll('div').at(2).classes()).toContain('nextProposition');
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('import exist', () => {
    expect(topProposition.length > 0);
  });
});

