import Badge from '@/components/atoms/Badge';

const CARD = 'card';
const CARD_GOLD = 'card-gold';
const CARD_SILVER = 'card-silver';
const CARD_BRONZE = 'card-bronze';

const name = 'ContractorCard';
const props = ['dataModel'];

export default {
  name,
  props,
  components: {
    Badge,
  },
  data() {
    return {
      defaultLogoImg: 'this.src= " ../../../../static/img/IPRO_logo.png" ',
    };
  },
  methods: {
    profileLink(dataModel) {
      if (dataModel.contractor_id || dataModel.contractor_id === 0) {
        return `/profile/${dataModel.contractor_id}`;
      }
      return '#';
    },
    classObjects() {
      const membershipProps = {
        Gold: CARD_GOLD,
        Silver: CARD_SILVER,
        Bronze: CARD_BRONZE,
      };
      if (!Object.prototype.hasOwnProperty.call(
        membershipProps,
        this.dataModel.membership,
      )) {
        return CARD;
      }
      return membershipProps[this.dataModel.membership];
    },
  },
};
