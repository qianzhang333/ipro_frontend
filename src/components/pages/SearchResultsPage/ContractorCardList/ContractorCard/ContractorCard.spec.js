import { shallowMount } from '@vue/test-utils';
import ContractorCard from './';

describe('ContractorCard.vue', () => {
  it('matches the snapshot of the ContractorCard which membership is Gold', () => {
    const goldDataModel = {
      contractor_id: '1',
      countryAddr: 'Australia',
      companyLogo: 'https://image.freepik.com/free-vector/abstract-company-logo-template_1043-191.jpg',
      legalEntityName: 'Vendora Pty Ltd',
      tradingName: 'Vendora',
      serviceCategories: 'Consultants - General',
      membership: 'Gold',
      suburbAddr: '1',
      postcode: '2',
      streetAddr: '3',
      numberAddr: '4',
      stateAddr: 'VIC',
      phoneNumber: '411111111',
      email: 'enquiries@vendora.com.au',
      status: 'application_in_progress',
    };
    const wrapper = shallowMount(ContractorCard, {
      propsData: {
        dataModel: goldDataModel,
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches the snapshot of the ContractorCard which membership is Silver', () => {
    const silverDataModel = {
      membership: 'Silver',
    };
    const wrapper = shallowMount(ContractorCard, {
      propsData: {
        dataModel: silverDataModel,
      },
    });
    expect(wrapper.contains('.card-silver')).toBe(true);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches the snapshot of the ContractorCard which membership is Bronze', () => {
    const bronzeDataModel = {
      membership: 'Bronze',
    };
    const wrapper = shallowMount(ContractorCard, {
      propsData: {
        dataModel: bronzeDataModel,
      },
    });
    expect(wrapper.contains('.card-bronze')).toBe(true);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches snapshot the ContractorCard which does not have a membership', () => {
    const cardDataModel = {
      membership: '',
    };
    const wrapper = shallowMount(ContractorCard, {
      propsData: {
        dataModel: cardDataModel,
      },
    });
    expect(wrapper.contains('.card')).toBe(true);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches snapshot the ContractorCard which does not import a object data', () => {
    const nullCardDataModel = {
    };
    const wrapper = shallowMount(ContractorCard, {
      propsData: {
        dataModel: nullCardDataModel,
      },
    });
    expect(wrapper.contains('.card-bronze')).toBe(false);
    expect(wrapper.contains('.card-silver')).toBe(false);
    expect(wrapper.contains('.card-gold')).toBe(false);
    expect(wrapper.contains('.card')).toBe(true);
    expect(wrapper.html()).toMatchSnapshot();
  });
});

