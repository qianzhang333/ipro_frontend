import { generalQuery } from '@/utils/searchApi';
import ContractorCard from './ContractorCard';

const name = 'contractor-card-list';

export default {
  name,
  props: ['input'],
  components: {
    ContractorCard,
  },
  data() {
    return {
      contractor: [],
    };
  },
  computed: {
    searchInput() {
      return this.input;
    },
  },
  apollo: {
    // No alternative way found to pass the parameters to generalQuery()
    contractor: {
      query() {
        // return the query part
        return generalQuery(this.searchInput).contractor.query;
      },
      variables() {
        // return the variables part
        return generalQuery(this.searchInput).contractor.variables;
      },
    },
  },
};
