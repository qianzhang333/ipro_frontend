import { shallowMount } from '@vue/test-utils';
import ContractorCardList from './';

describe('ContractorCardList.vue', () => {
  it('should render all contractor cards and has length of 55', () => {
    const wrapper = shallowMount(ContractorCardList);
    expect(wrapper.html()).toMatchSnapshot();
  });
});
