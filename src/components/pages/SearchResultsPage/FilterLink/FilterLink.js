export default {
  name: 'FilterLink',
  props: ['node'],
  methods: {
    displayName() {
      if (this.node) {
        if (this.node.count) {
          return `${this.node.name} (${this.node.count})`;
        }
        return this.node.name;
      }
      return null;
    },
  },
};
