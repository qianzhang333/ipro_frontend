import { shallowMount } from '@vue/test-utils';
import FilterLink from './';

describe('FilterLink.vue', () => {
  it('matches when the node is undefined', () => {
    const wrapper = shallowMount(FilterLink, {});
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches when the node does not have any count', () => {
    const dataCountNull = {
      name: 'dataCountNull',
    };
    const wrapper = shallowMount(FilterLink, {
      propsData: {
        node: dataCountNull,
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches when the count of a node is 0', () => {
    const dataCount0 = {
      name: 'data_count_0',
      count: 0,
    };
    const wrapper = shallowMount(FilterLink, {
      propsData: {
        node: dataCount0,
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('matches when node has a count greater than or equal to 1', () => {
    const dataCount = {
      name: 'data_count',
      count: 5,
    };
    const wrapper = shallowMount(FilterLink, {
      propsData: {
        node: dataCount,
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
