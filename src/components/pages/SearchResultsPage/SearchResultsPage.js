import router from '@/router';
import NavHeader from '@/components/organisms/NavHeader';
import Search from '@/components/organisms/Search';
import NavFooter from '@/components/organisms/NavFooter';
import ContractorCardList from './ContractorCardList';
import FilterLink from './FilterLink';
import filterData from './filter-data.json';

const name = 'SearchResultsPage';

export default {
  name,
  components: {
    NavHeader,
    Search,
    FilterLink,
    ContractorCardList,
    NavFooter,
  },
  methods: {
    getInput() {
      // return the current route's name parameter, which follows results/:name
      return router.currentRoute.params.name;
    },
  },
  data() {
    return {
      filterNode: {
        name: 'Filters',
        children: filterData.data,
      },
    };
  },
};
