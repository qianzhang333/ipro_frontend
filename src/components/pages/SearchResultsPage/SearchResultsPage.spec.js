import { shallowMount } from '@vue/test-utils';
import SearchResultPage from './';

describe('<SearchResultPage.vue />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(SearchResultPage, {});
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('import exist', () => {
    expect(SearchResultPage.length > 0);
  });
});
