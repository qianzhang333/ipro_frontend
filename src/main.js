import '@babel/polyfill';
import Vue from 'vue';
import VueResource from 'vue-resource';
import Vuetify from 'vuetify';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';
import App from './components/App';
import router from './router';
import theme from './utils/theme';

Vue.config.productionTip = false;

Vue.use(Vuetify, { theme });
Vue.use(VueResource);

const httpLink = new HttpLink({
  // uri: 'http://localhost:3020/graphql',
  uri: 'https://iproconnect.herokuapp.com/v1alpha1/graphql',
});

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
  connectToDevTools: true,
});

Vue.use(VueApollo);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  provide: apolloProvider.provide(),
  components: { App },
  template: '<App/>',
});
