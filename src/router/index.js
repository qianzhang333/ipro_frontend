import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/components/pages/HomePage';
import SearchResult from '@/components/pages/SearchResultsPage';
import Contractor from '@/components/pages/ContractorProfilePage';
import ErrorPage from '@/components/pages/ErrorPage';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage,
    },
    {
      path: '/results/:name',
      name: 'SearchResults',
      component: SearchResult,
    },
    {
      path: '/profile/:id',
      name: 'Profile',
      component: Contractor,
    },
    {
      path: '/404',
      name: 'ErrorPage',
      component: ErrorPage,
    },
    {
      path: '/Contact',
      beforeEnter() {
        window.location = 'https://www.ipro.net/about/#contact';
      },
    },
    {
      path: '/Privacy Policy',
      beforeEnter() {
        window.location = 'https://www.ipro.net/privacy-policy/';
      },
    },
    {
      path: '/User Terms & Conditions',
      beforeEnter() {
        window.location = 'https://www.ipro.net/terms-and-conditions/';
      },
    },
    {
      path: '/login',
      beforeEnter() {
        window.location = 'http://app.ipro.net ';
      },
    },
    {
      path: '/signup',
      beforeEnter() {
        window.location = 'http://app.ipro.net ';
      },
    },
    {
      path: '/becontractor',
      beforeEnter() {
        window.location = 'http://app.ipro.net ';
      },
    },
    {
      path: '*',
      redirect: '/404',
    },
  ],
});
