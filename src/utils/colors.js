const colors = {
  BLACK: '#000',
  BLACKCURRANT: '#0d0e21',
  ELF_GREEN: '#1B967F',
  PAUA: '#1E2045',
  BLUECHILL: '#3c9282',
  PUERTO_RICO: '#52c0ac',
  ICE_COLD: '#aadfd5',
  DARK_GREY: '#a7a7a7',
  LIGHT_GREY: '#D4D4D4',
  FLAMINGO: '#e05353',
  CONFETTI: '#e0c03',
  SOLITUDE: '#e1e8ec',
  ALICE_BLUE: '#E9F7FF',
  MUSTARD: '#FFDB48',
  WHITE: '#FFFFFF',
  CRIMSON: '#dc0f27',
};

export default colors;
