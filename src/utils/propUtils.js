const getBooleanAttribute = (vueObject, propList, defaultProp) => {
  if (vueObject.$options === undefined) { return null; }
  const vueProps = vueObject.$options.propsData;
  let resultProp = defaultProp;
  if (vueProps !== undefined) {
    propList.some((prop) => {
      const isPropInVue = (prop in vueProps);
      if (isPropInVue) {
        resultProp = prop;
      }
      return isPropInVue;
    });
  }
  return { [resultProp]: true };
};

export default getBooleanAttribute;
