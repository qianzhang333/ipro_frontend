import Vue from 'vue';
import getBooleanAttribute from './propUtils';

describe('getBooleanAttibute', () => {
  it('propsdata exist', () => {
    const vm = new Vue({
      propsData: {
        h1: '',
      },
    });
    expect(getBooleanAttribute(vm, ['h1', 'h2', 'h3'], 'h3').h1).toBe(true);
    expect(getBooleanAttribute(vm, ['h2', 'h3'], 'h3').h3).toBe(true);
  });
  it('props not exist', () => {
    const vm = new Vue();
    expect(getBooleanAttribute(vm,
      ['h2', 'h3'],
      'h3').h3).toBe(true);
  });
});
