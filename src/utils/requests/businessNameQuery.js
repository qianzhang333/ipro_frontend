import gql from 'graphql-tag';

const BUSINESS_NAME_QUERY = gql`query ($searchText: String, $limit: Int) {
  contractor (
    where: {trading_name: {_like: $searchText}},
    limit: $limit
  ) {
    contractor_id
    trading_name
  }
  service_category (
    where: {category: {_like: $searchText}},
    limit: $limit
  ) {
    category
  }
}`;

export default BUSINESS_NAME_QUERY;
