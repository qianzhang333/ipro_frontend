import gql from 'graphql-tag';

const GENERAL_QUERY = gql`query contractor 
  ($input: String, $membership: String) {
    contractor(where: {
      _and: [
        {
          _or: [
          {membership: {_like: $membership}},
          {membership: {_similar: $membership}}]
        },
        {
          _or: [
            {
              _or: [
              {trading_name: {_like: $input}},
              {trading_name: {_similar: $input}}]
            },
            {
              _or: [
              {industry: {industry: {_like: $input}}},
              {industry: {industry: {_similar: $input}}}]
            },
            {
              _or: [
              {service_category: {category: {_like: $input}}},
              {service_category: {category: {_similar: $input}}}]
            }
          ]
        }
      ]

  }) {
    contractor_id
    abn
    status
    legal_entity_name
    trading_name
    description
    display_logo
    membership
    rating
    phone_number
    email
    industry {
      industry_id
      industry
    }
    service_category {
      service_category_id
      category
    }
    location {
      location_id
      site
      state
      region
      council
      suburb
      precinct
      property
    }
    serviced_area {
      serviced_area_id
      longitude
      latitude
      radius
    }
  }
}`;

export default GENERAL_QUERY;
