import gql from 'graphql-tag';

const INTERSECTION_QUERY = gql`query contractor 
  ($membership: String, $name: String, $category: String, $industry: String) {
    contractor(where: {
      _and: [
        {
          _or: [
          {membership: {_like: $membership}},
          {membership: {_similar: $membership}}]
        },
        {
          _or: [
          {trading_name: {_like: $name}},
          {trading_name: {_similar: $name}}]
        },
        {
          _or: [
          {industry: {industry: {_like: $industry}}},
          {industry: {industry: {_similar: $industry}}}]
        },
        {
          _or: [
          {service_category: {category: {_like: $category}}},
          {service_category: {category: {_similar: $category}}}]
        }
      ]
  }) {
    contractor_id
    abn
    status
    legal_entity_name
    trading_name
    description
    display_logo
    membership
    rating
    phone_number
    email
    industry {
      industry_id
      industry
    }
    service_category {
      service_category_id
      category
    }
    location {
      location_id
      site
      state
      region
      council
      suburb
      precinct
      property
    }
    serviced_area {
      serviced_area_id
      longitude
      latitude
      radius
    }
  }
}`;

export default INTERSECTION_QUERY;
