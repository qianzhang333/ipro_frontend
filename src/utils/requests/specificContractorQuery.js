import gql from 'graphql-tag';

const CONTRACTOR_QUERY = gql`query ($contractorId: uuid) {
  contractor (where: {contractor_id: {_eq: $contractorId}}) {
    contractor_id
    abn
    status
    legal_entity_name
    trading_name
    description
    display_logo
    membership
    rating
    phone_number
    email
    # industry category
    industry {
      industry_id
      industry
    }
    # service category
    service_category {
      service_category_id
      category
    }
    # location
    location {
      location_id
      site
      state
      region
      council
      suburb
      precinct
      property
    }
    # serviced area
    serviced_area {
      serviced_area_id
      longitude
      latitude
      radius
    }
  }
}`;

export default CONTRACTOR_QUERY;
