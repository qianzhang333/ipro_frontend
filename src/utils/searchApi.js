import CONTRACTOR_QUERY from './requests/specificContractorQuery';
import GENERAL_QUERY from './requests/generalQuery';
import INTERSECTION_QUERY from './requests/intersectionQuery';
import BUSINESS_NAME_QUERY from './requests/businessNameQuery';

// This is the General intersection one
function intersectionQuery(membership, name, category, industry) {
  let newMembership = '%';
  let newName = '%';
  let newCategory = '%';
  let newIndustry = '%';
  if (typeof membership !== 'undefined') {
    newMembership = `%${membership}%`;
  }
  if (typeof name !== 'undefined') {
    newName = `%${name}%`;
  }
  if (typeof category !== 'undefined') {
    newCategory = `%${category}%`;
  }
  if (typeof industry !== 'undefined') {
    newIndustry = `%${industry}%`;
  }
  return {
    contractor: {
      query: INTERSECTION_QUERY,
      variables: {
        membership: newMembership,
        name: newName,
        category: newCategory,
        industry: newIndustry,
      },
    },
  };
}

// Search General Results in Name Category and Industry,
// Which can also get the membership filter
function generalQuery(input, membership) {
  let newInput = '%';
  let newMembership = '%';
  if (typeof input !== 'undefined') {
    newInput = `%${input}%`;
  }
  if (typeof membership !== 'undefined') {
    newMembership = `%${membership}%`;
  }
  return {
    contractor: {
      query: GENERAL_QUERY,
      variables: {
        input: newInput,
        membership: newMembership,
      },
    },
  };
}

function contractorQuery(contractorId, onSuccess, onError) {
  return {
    contractor: {
      query: CONTRACTOR_QUERY,
      variables: {
        contractorId,
      },
      result(result) {
        if (onSuccess) {
          onSuccess(result);
        }
      },
      error(error) {
        if (onError) {
          onError(error);
        }
      },
    },
  };
}

// Used for requesting search suggestion (auto-completion)
function businessNameQuery(params) {
  return {
    contractor: {
      query: BUSINESS_NAME_QUERY,
      variables: {
        searchText: params.searchText,
        limit: params.limit,
      },
      result(result) {
        if (params.onSuccess) {
          params.onSuccess(result);
        }
      },
      error(error) {
        if (params.onError) {
          params.onError(error);
        }
      },
    },
  };
}

export {
  intersectionQuery,
  generalQuery,
  contractorQuery,
  businessNameQuery,
};
