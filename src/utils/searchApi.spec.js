import { generalQuery, intersectionQuery, contractorQuery } from './searchApi';

describe('searchApi Object', () => {
  it('contains searchApi', () => {
    expect(generalQuery().contractor > 0);
    expect(intersectionQuery().contractor > 0);
    expect(contractorQuery().contractor > 0);
    expect(generalQuery('', 'a').contractor.variables.membership = '%a%');
    expect(generalQuery('a', '').contractor.variables.input = '%a%');
    expect(intersectionQuery('a').contractor.variables.membership = '%a%');
    expect(intersectionQuery('', 'a').contractor.variables.name = '%a%');
    expect(intersectionQuery('', '', 'a').contractor.variables.category = '%a%');
    expect(intersectionQuery('', '', '', 'a').contractor.variables.industry = '%a%');
  });
});
