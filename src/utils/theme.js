import colors from './colors';

const backgroundIndexPrimaryColor = '#000000';

export default {
  backgroundIndexPrimaryColor,
  primary: colors.PUERTO_RICO,
  secondary: backgroundIndexPrimaryColor,
  accent: backgroundIndexPrimaryColor,
  error: colors.FLAMINGO,
  info: colors.ALICE_BLUE,
  success: colors.ELF_GREEN,
  warning: colors.MUSTARD,
};
