module.exports = {
  TestContractorDetail(browser) {
    // const devServer = browser.globals.devServerURL;
    browser
      .url('http://localhost:8080/profile/1')
      // .pause(1000)
      .waitForElementVisible('body', 1) // assure homePage is presenting
      .saveScreenshot('./screenshots/homePage.png')// take a screen shot of home page
      .waitForElementVisible('.nav-button', 1)
      .assert.containsText('nav', 'BECOME A CONTRACTOR')// assure NavHeader is presenting
      .assert.containsText('nav', 'SIGN UP')// assure NavHeader is presenting
      .assert.containsText('nav', 'LOGIN')// assure NavHeader is presenting
      // .assert.containsText('btn__content','Back To Search Result')
      // assure NavHeader is presenting
      .waitForElementVisible('#ContractorProfileCard', 1)
      .assert.containsText('#ContractorProfileCard', 'First Response Plumbing')
      // assure NavHeader is presenting
      // .assert.containsText('#about','First Response Plumbing')
      // .assert.containsText('app','Join to Invite')
      // assure NavHeader is presenting
      .click('id', 'btn-back-to-search')
      .waitForElementVisible('body', 1000)
      .pause(1000)
      .click('id', 'btn-join-to-invite')
      .waitForElementVisible('body', 1000)
      .pause(1000)
      .click('name', 'homePage')
      .waitForElementVisible('body', 1000)
      .pause(1000)
      .assert.containsText('h1', 'More than 1 Million Contractors')
      .setValue('id', 'input-primary', 'plumber')
      .pause(1000)
      .setValue('id', 'input-secondary', 'Sydney')
      .pause(1000)
      .click('name', 'searchResults')
      .waitForElementVisible('body', 1000) // ensure results button is working
      .assert.containsText('h1', 'Found 666 results')
      .pause(2000)
      .end();
  },
};
