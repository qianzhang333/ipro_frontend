module.exports = {
  TestHomePage(browser) {
    const devServer = browser.globals.devServerURL;
    browser
      .url(devServer)
      // .pause(1000)
      .waitForElementVisible('body', 1) // assure homePage is presenting
      .saveScreenshot('./screenshots/homePage.png')// take a screen shot of home page
      .waitForElementVisible('.nav-button', 1)
      .assert.containsText('nav', 'BECOME A CONTRACTOR')// assure NavHeader is presenting
      .assert.containsText('nav', 'SIGN UP')// assure NavHeader is presenting
      .assert.containsText('nav', 'LOGIN')// assure NavHeader is presenting
      // .assert.containsText('v-btn','Contractors')  // assure Contractors is presenting
      .setValue('id', 'primary-input', 'plumber')
      .setValue('id', 'secondary-input', 'Sydney')
      .pause(2000)
      .click('id', 'search-btn')
      .waitForElementVisible('body', 1000)
      // assure all the buttons are working
      .click('id', 'signup')
      .waitForElementVisible('body', 1000)
      .saveScreenshot('./screenshots/signuppage.png')// take a screen shot of sign up page
      .click('id', 'Login')
      .waitForElementVisible('body', 1000)
      .click('name', 'searchResults')
      .waitForElementVisible('body', 1000) // ensure results button is working
      .assert.containsText('h1', 'Header (remove me)')
      .pause(5000)
      .end();
  },
};
