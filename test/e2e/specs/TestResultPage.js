module.exports = {
  TestResultPage(browser) {
    browser
      .url('http://localhost:8080/results/21')
      .pause(1000)
      .waitForElementVisible('body', 5000)
      .elements('id', 'ContractorCard', function result() { // ensure the list contains 55 items
        browser.assert.equal(result.value.length, 55);
        // console.log(elements.ELEMENTS);
      })
      .elements('class name', 'img-container', function result() { // ensure the img is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .elements('class name', 'company-logo', function result() { // ensure the company-logo is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .elements('class name', 'card-detail', function result() { // ensure the card-detail is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .elements('class name', 'company-name', function result() { // ensure the company-name is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .elements('class name', 'company-location', function result() { // ensure the company-location is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .elements('class name', 'company-category', function result() { // ensure the company-category is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .elements('class name', 'badge-img', function result() { // ensure the badge-img is correctly presented
        browser.assert.equal(result.value.length, 55);
      })
      .saveScreenshot('./screenshots/Resultpage.png') // take a screen shot of result page
      .click('name', 'homePage')
      .waitForElementVisible('nav', 1000) // ensure the home button is working
      .assert.containsText('h1', 'Contractors')
      .pause(1000)
      .end();
  },
};
