import Vue from 'vue';
import Vuetify from 'vuetify';
import Router from 'vue-router';

// Required to resolve `TypeError: Object.values is not a function` when
// `Vue.use(Vuetify)` is called.
Object.values = obj => Object.keys(obj).map(key => obj[key]);
Vue.use(Vuetify);
Vue.use(Router);
Vue.config.silent = true;
Vue.config.productionTip = false;

function addElemWithDataAppToBody() {
  const app = document.createElement('div');
  app.setAttribute('data-app', true);
  document.body.append(app);
}
addElemWithDataAppToBody();
